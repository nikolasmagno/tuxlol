﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace Xargoth.TuxLoL.Core.Utility
{
    internal static class StreamExtensions
    {
        internal static void CopySection(this Stream stream, Stream destination, int offset, int count)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            if (destination == null)
            {
                throw new ArgumentNullException("destination");
            }

            // TODO: More validation


            stream.Position = offset;
            int bytesCopied = 0;
            var buffer = new byte[4 * 1024];
            do
            {
                int bytesNeeded = count - bytesCopied;
                int bytes = stream.Read(buffer, 0, bytesNeeded < buffer.Length ? bytesNeeded : buffer.Length);
                destination.Write(buffer, 0, bytes);
                bytesCopied += bytes;
            } while (bytesCopied < count);
        }
    }
}