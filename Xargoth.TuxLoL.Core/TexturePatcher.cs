﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;

namespace Xargoth.TuxLoL.Core
{
    public class TexturePatcher : IDisposable
    {
        private readonly BinaryReader reader;
        private readonly BinaryWriter writer;
        private bool disposed;

        public TexturePatcher(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            reader = new BinaryReader(stream);
            writer = new BinaryWriter(stream);
        }

        public TexturePatcher(byte[] data) : this(new MemoryStream(data))
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public bool CanPatch()
        {
            reader.BaseStream.Position = 84;
			int format = reader.ReadInt16();
			if (format == 0x5844) // DXT format
            {
                reader.BaseStream.Position = 28;
                int mipmapCount = reader.ReadInt32();
                return mipmapCount > 1;
            }

            return false;
        }

        public void Patch()
        {
            writer.BaseStream.Position = 28;
            writer.Write(1);
        }

        ~TexturePatcher()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                reader.Dispose();
                writer.Dispose();
            }

            disposed = true;
        }
    }
}