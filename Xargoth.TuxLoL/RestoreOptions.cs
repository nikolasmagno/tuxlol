﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using CommandLine;

namespace Xargoth.TuxLoL
{
    [Verb("restore", HelpText = "Revert the League of Legends patches.")]
    internal class RestoreOptions
    {
        [Option('d', "dir", HelpText = "The path to the League of Legends directory.", Required = true)]
        public string LoLDirectory { get; set; }
    }
}